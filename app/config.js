/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: global configuration settings 
*/


/***************** Environment specific settings *******************/


/* Environment */
var APIURL = "https://api.scribesoft.com/v1/orgs/";
// API credentials - Only hard code for testing, these values are taken from the Index.html form
var APIUSERNAME = "";
var APIPASSWORD = "";
var PARENTORGROOT = ""; //If this is empty traverse all orgs 

/* End Environment */



 