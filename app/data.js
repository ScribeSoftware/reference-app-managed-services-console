/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


// Contents: functions for calling Scribe APIs

//301.2 version updated to remove API Security Access Check as this is now in the API  ?filterByAPIAccess=true

function getAgents(orgId, orgName) {
    var APIURL = "https://api.scribesoft.com:443/v1/orgs/" + orgId + "/agents"; 
    var agents =[]; 
    var agent; 
    var dataSet; 
    var lastRun; 

  return new Promise(function(resolve, reject) {

    var data = $.get({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        }
    })
    
    

    .done(function(results) {
               
                for (var i = 0; i < results.length; i++) {
                    agent = results[i];
                    //Push a simple Solution object

                  if (agent.lastContactTime !== null){
                      lastRun = moment(agent.lastContactTime).format('M/D/YYYY h:mm A'); 
                    }
                    else{
                      lastRun = null; 
                    }


                    agents.push({org: orgName, 
                                id: agent.id, 
                                name: agent.name, 
                                lastContactTime: lastRun, 
                                machineName: agent.machineName, 
                                status: agent.status });   
               
                }
        
        resolve(agents);
        
               
        })
        .fail(function(xhr) {
                console.log('error', xhr);
            });   
    
  });

}


function getHistories(orgId, solutionId, name) {
    var activeSolutionId = solutionId; 
    var solution = name; 
    var history; 
    var start; 
    var histories =[]; 
    var APIURL = "https://api.scribesoft.com:443/v1/orgs/" + orgId + "/solutions/" + activeSolutionId + "/history"; 
    
    return new Promise(function(resolve, reject) {
        var data = $.get({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        }
    })
    
    .done(function(results) {
               
                for (var i = 0; i < results.length; i++) {
                    history = results[i];
                    //Push a simple history object

                  if (history.start !== null){
                      start = history.start;
                     }
                    else{
                      start = null; 
                    }

                     histories.push({orgId: orgId, 
                                solutionId: activeSolutionId, 
                                solution: solution, 
                                historyId: history.id, 
                                recordsFailed: history.recordsFailed, 
                                result: history.result, 
                                details: history.details, 
                                start: start});  
                     
        }
        resolve(histories);
        
               
        })
        .fail(function(xhr) {
                console.log('error', xhr);
            });   
    
  });

}


function getOrgs() {
    //Only return ORGS that can be accessed via being Open To current Callers IP Address
    var APIURL = "https://api.scribesoft.com:443/v1/orgs?filterByAPIAccess=true"; 
    var primaryContact; 
    var orgs =[]; 
    var org; 
    var dataSet; 
    

    return new Promise(function(resolve, reject) {

    var data = $.get({
                type: "GET",
                 url: APIURL,
                headers: { "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)}
                })
    
        .done(function(results) {
        
                for (var i = 0; i < results.length; i++) {
                    org = results[i];
                  
                    primaryContact = org.primaryContactFirstName + ' ' + org.primaryContactLastName; 
                    //Push a simple ORG object

                    orgs.push({id: org.id,name: org.name,primaryContact: primaryContact,parentId: org.parentId });   
                    
                }

        resolve(orgs);       
        })

        .fail(function(xhr) {
                console.log('error', xhr);
        });   
    });
}


function getRecords(orgId, solutionId, historyId) {
    var activeSolutionId = solutionId; 
    var record; 
    var start; 
    var records =[]; 
    var APIURL = "https://api.scribesoft.com:443/v1/orgs/" + orgId + '/solutions/' + activeSolutionId + '/history/' + historyId + "/errors"; 
    
    return new Promise(function(resolve, reject) {
        var data = $.get({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        }
    })
    
    .done(function(results) {
               
                for (var i = 0; i < results.length; i++) {
                    record = results[i];
                    //Push a simple error record object
                     records.push({mapName: record.mapName, 
                                  sourceEntity: record.sourceEntity, 
                                  errorMessage: record.errorMessage, 
                                  errorDetail: record.errorDetail, 
                                  additionalErrorDetails: record.additionalErrorDetails});  
                     
        }
        resolve(records);
        
               
        })
        .fail(function(xhr) {
                console.log('error', xhr);
            });   
    
  });

}


function getSolutions(orgId, orgName) {
    var APIURL = "https://api.scribesoft.com:443/v1/orgs/" + orgId + "/solutions"; 
    var solutions =[]; 
    var solution; 
    var dataSet; 
    var lastRun; 

    return new Promise(function(resolve, reject) {

    var data = $.get({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        }
    })
    

    .done(function(results) {
            
                for (var i = 0; i < results.length; i++) {
                    solution = results[i];
            
                    if (solution.lastRunTime !== null){
                      lastRun = moment(solution.lastRunTime).format('M/D/YYYY h:mm A'); 
                      //lastRun = solution.lastRunTime; 
                    }
                    else{
                      lastRun = null; 
                    }

                    solutions.push({org: orgName, 
                                    orgId: orgId, 
                                    id: solution.id, 
                                    name: solution.name, 
                                    solutionType: solution.solutionType, 
                                    agentId: solution.agentId, 
                                    lastRunTime: solution.lastRunTime,
                                    local: lastRun, 
                                    status: solution.status });   
                  }
        
        resolve(solutions);
      })
      
      .fail(function(xhr) {
         console.log('error', xhr);
        });   
  });

}

//ARRAY BUILDING FUNCTIONS
function appendAgents(data, agents){
  //Append the Solutions Object to the Data Set and return the Data Set
  var agent; 

  for (var i = 0; i < agents.length; i++) {
      agent = agents[i]; 
      data.push({org: agent.org, 
                id: agent.id, 
                name: agent.name, 
                machine: agent.machineName, 
                status: agent.status, 
                lastContact: agent.lastContactTime});  
    }

return data; 

}


function appendErrors(data, solutions){
//Append the Solutions Object to the Data Set and return the Data Set
  var solution; 

  for (var i = 0; i < solutions.length; i++) {
         solution = solutions[i]; 
         
         if (solution.status == "IdleLastRunFailed" || 
            solution.status == "IdleLastRunRowErrors" ||
            solution.status == "OnDemandLastRunFailed" ||
            solution.status == "OnDemandLastRunRowErrors") 
            {
                data.push({"orgName": solution.org, "orgId": solution.orgId, "solutionId": solution.id, name: solution.name, solutionType: solution.solutionType, status: solution.status, agentId: solution.agentId });  
            }
  }

return data; 

}


function appendSolutions(data, solutions){
  var solution; 

  for (var i = 0; i < solutions.length; i++) {
         solution = solutions[i]; 
    
         data.push({org: solution.org, 
                    id: solution.id, 
                    name: solution.name, 
                    solutionType: solution.solutionType, 
                    status: solution.status, 
                    lastRunTime: solution.lastRunTime,
                    agentId: solution.agentId });  
    }

return data; 

}

