/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for Connecting to the Scribe Platform 
*/

function getConnected() {
    //Clears information about the logged in USER
    var divSuccess = $('#divLoginUser').find('.alert-success');
    var divFailure = $('#divLoginUser').find('.alert-danger');
    divSuccess.empty();
    divSuccess.slideUp();
    divFailure.empty();
    divFailure.slideUp();

     //Get the Credentials from the FORM
     APIUSERNAME = $('#usr').val();
     APIPASSWORD = $('#pwd').val();

    //BIND to the Scribe ORGS Service 
    $.ajax({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        data: { },
        dateType: 'json',
        success: function (result) {
             var divSuccess = $('#divLoginUser').find('.alert-success');
             divSuccess.append("Session Connected for " + APIUSERNAME);
             divSuccess.slideDown();  

        //Activate the Disabled Navigation Tabs/Pills
        $('.nav li').removeClass('disabled');
        //Enable Navigation;    
        $('.nav li').find('a').attr("data-toggle","tab"); 

        },

        error: function (xhr, error) {
            //Add code to disable the click on associated tabs
            //TODO ITEM 

            console.log(xhr);
            console.log(error);
            var divFailure = $('#divLoginUser').find('.alert-danger');
            divFailure.append("Unable to connect " + APIUSERNAME );
            $('#pwd').val("");
            divFailure.slideDown();
            //Ensure that all non active Navigation Tabs/Pills become disabled
            $('.nav li').not('.active').addClass('disabled');
            //Disable Navigation;    
            $('.nav li').not('.active').find('a').removeAttr("data-toggle");


        
        }
    });
}

