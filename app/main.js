/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: global variables, document ready initialization for click handlers.  Utility functions.
*/


/* Document Ready callback - wire up button click events after the DOM has loaded
   Also do html initialization which currently is just setting the parent orgid for the step 1 label.
*/
$(document).ready(function () {
    $('#get-connected').on('click', function () {
        getConnected();    
    });   

 //First Bind the Tabs Collection to the Tabs Event Handlers   

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href"); 

        if(target && target.toString().indexOf('Solutions') >= 0){
            mspSolutionSurface();
        }
        else if (target && target.toString().indexOf('Agents') >= 0){
            mspAgentSurface();
        }
        else if (target && target.toString().indexOf('Focus') >= 0){
            mspFocusSurface(); 
        }
    });  

    //For the non-active tabs remove the data toggle attribute 
    //Add it back in the Auth Success handler 
    $('.nav li').not('.active').find('a').removeAttr("data-toggle");
});

function mspAgentSurface(){
   var org; 
   var orgs;
   var data =[];
   var agent; 

   // Get the Orgs by Parent ID for now get all availalble
    getOrgs().then(function(orgs){
   // Get All Solutions for each Included Child Org including the Agent and Agent Health for each row

   for (var i = 0; i < orgs.length; i++) {
          getAgents(orgs[i].id, orgs[i].name).then(function(agents){
          appendAgents(data,agents);
          mspAgentOutput(data);  
     });

   }    

   // Binds the Result set the Presentation Output 
     
    }).catch(function(v){
        // Process the Error appropriately
    });     
}


function mspFocusSurface(){
   var org; 
   var orgs;
   var data =[];
   var solution; 

   // Get the Orgs by Parent ID for now get all availalble
    getOrgs().then(function(orgs){
   // Get All Solutions for each Included Child Org including the Agent and Agent Health for each row

   for (var i = 0; i < orgs.length; i++) {
         // org = orgs[i].name
          getSolutions(orgs[i].id, orgs[i].name).then(function(solutions){
          appendErrors(data, solutions);
          mspFocusOutput(data); 
     });   

   }    
   // Binds the Result set the Presentation Output 
   
    }).catch(function(v){
        // Process the Error appropriately
    });     
}


function mspSolutionSurface(){
   var org; 
   var orgs;
   var data =[];
   var solution; 

   // Get the Orgs by Parent ID for now get all availalble
  getOrgs().then(function(orgs){
   // Get All Solutions for each Included Child Org including the Agent and Agent Health for each row

   for (var i = 0; i < orgs.length; i++) {
        getSolutions(orgs[i].id, orgs[i].name).then(function(solutions){
        appendSolutions(data, solutions);
        mspSolutionOutput(data);
     });
   }    
   // Binds the Result set the Presentation Output      
    }).catch(function(v){
        // Process the Error appropriately
    });

}


function focusHistory(e){
        //Get the Focus Selected Item from the GRID.  
        //Use this item to drill into the related history records
        //output the history records to child grid 
        var data =[]; 
        var histories = [];  
        //Clear the Sub Grids
        clearGrid("#divSolutionHistoryErrorsCollection"); 
        clearGrid("#jsGridErrorFatals"); 
        clearGrid("jsGridErrorFatals"); 
        $('#divFatalErrors').hide(); 
        $('#divdivRecordErrors').hide(); 
        histories = getHistories(e.item.orgId, e.item.solutionId, e.item.name).then(function(histories){
        mspHistoriesOutput(histories);
    });
 }

function focusRecords(e){
        var data =[]; 
        var records = [];  
        records = getRecords(e.item.orgId, e.item.solutionId, e.item.historyId).then(function(records){
        mspRecordsOutput(records);
    });
 }

/******************************************** Utility functions ************************************************/


/* Function to loop through array and find a key match
 */
function findValueForConnectionPropertyKey(keyName, myArray) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].key === keyName) {
            return myArray[i].value;
        }
    }
}

/* Utility function to normalize the Scribe data type for display purposes
*/

function normalizeDataType(dataType, size) {
    var normalizedType;
    if (!dataType) {
        return "";
    }

    if (dataType.indexOf('System.') !== -1) {
        dataType = dataType.slice(dataType.indexOf('.') + 1, dataType.length);
    }
    dataType = dataType.toLowerCase();
    switch (dataType) {
        case "int16":
            normalizedType = "Integer16";
            break;
        case "int32":
            normalizedType = "Integer32";
            break;
        case "int64":
            normalizedType = "Integer64";
            break;
        case "uint16":
            normalizedType = "UnsignedInteger16";
            break;
        case "uint32":
            normalizedType = "UnsignedInteger32";
            break;
        case "uint64":
            normalizedType = "UnsignedInteger64";
            break;
        case "decimal":
            normalizedType = "Decimal";
            break;
        case "double":
            normalizedType = "Double64";
            break;
        case "single":
            normalizedType = "Double32";
            break;
        case "boolean":
            normalizedType = "Boolean";
            break;
        case "datetime":
            normalizedType = "DateTime";
            break;
        case "timespan":
            normalizedType = "TimeSpan";
            break;
        case "guid":
            normalizedType = "Guid";
            break;
        case "string":
            if (size === 0)
                normalizedType = "String";
            else
                normalizedType = "String(" + size + ")";
            break;
        case "byte[]":
            if (size === 0)
                normalizedType = "Byte Array";
            else
                normalizedType = "Byte Array(" + size + ")";
            break;
        case "byte":
            if (size === 0)
                normalizedType = "Byte";
            else
                normalizedType = "Byte(" + size + ")";
            break;
        case "char":
            normalizedType = "Char";
            break;
        default:
        case "object":
            normalizedType = "Object";
            break;
    }

    return normalizedType;
}


/* Extend Math to round a number to certain number of decimal places */
(function () {
    /**
     * Decimal adjustment of a number.
     *
     * @param {String}  type  The type of adjustment.
     * @param {Number}  value The number.
     * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
     * @returns {Number} The adjusted value.
     */
    function decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Decimal round
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Decimal floor
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Decimal ceil
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();
